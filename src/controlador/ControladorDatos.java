/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.*;
import modelo.*;

/**
 *
 * @author Jhosh PC
 */
public class ControladorDatos {
    
    private ControladorDatos(){
    }
    
    private static final ControladorDatos controladorDatos = new ControladorDatos();
    
    private Hospital hospital;
    
    public static ControladorDatos getController(){
        return controladorDatos;
    }
    
    public void guardarObjeto(){
        try{
            FileOutputStream fileIn = new FileOutputStream("data/Hospital.loc");
            ObjectOutputStream in = new ObjectOutputStream(fileIn);
            in.writeObject(hospital);
            System.out.println("Registro Exitoso!");
            in.close();
            fileIn.close();
        }catch(IOException e){
            System.out.println("ERROR al Registrar" + e.getMessage());
        }
    }
    
    public void cargarHospital(){
        
      try{   
        FileInputStream fileIn = new FileInputStream("data/Hospital.loc");
         ObjectInputStream in = new ObjectInputStream(fileIn);
         hospital = (Hospital) in.readObject();
         System.out.println("Datos Cargados");
         in.close();
         fileIn.close();
      } catch (IOException i) {
          System.out.println(i.getMessage());
      } catch (ClassNotFoundException c) {
          System.out.println(c.getMessage());
      } finally {
            if(hospital == null)
            {
                hospital = new Hospital();
            }else{}
      }
      
    }
    
    public Hospital getHospital(){
        return hospital;
    }
    
    
    
}
