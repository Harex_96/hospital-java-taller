/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import vista.*;
import modelo.*;

/**
 *
 * @author Jhosh PC
 */
public class ControladorIngresos {
    
    private static final ControladorIngresos controladorIngresos = new ControladorIngresos();
    
    public static ControladorIngresos getController (){
        return controladorIngresos;
    }
    
    public boolean logIN(String user, String password){
        boolean respuesta = false;
        Paciente p = ControladorDatos.getController().getHospital().logeoPaciente(user,password);
        Doctor d = ControladorDatos.getController().getHospital().logeoDoctor(user,password);
        Administrador a = ControladorDatos.getController().getHospital().logeoAdministrador(user, password);
        
        if(p != null){
            PacienteJf v_paciente = new PacienteJf(p);
              
            v_paciente.setVisible(true);
            respuesta = true;
        }else{
            if(d != null){
                Doctores v_doctor = new Doctores(d);
                v_doctor.setVisible(true);
                respuesta = true;
            }
        }
        
        if(a != null) {
            AdminJf v_admin = new AdminJf();
            v_admin.setVisible(true);
            respuesta = true;
        }
        return respuesta;
    }
    
    
}

