/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.*;
import modelo.*;

/**
 *
 * @author DELL
 */
public class ControladorDoctores {
    
    private static final ControladorDoctores controladorDoctores = new ControladorDoctores();
    
    public static ControladorDoctores getController(){
        return controladorDoctores;
    }
    
    public boolean RegistrarDoctor(String nombre,String apellido,String nacimiento,String ci,String lugar,String sexo,String especialidad,String horario,int cantpacientes){
        boolean res = false;
        Doctor nuevoDoc = new Doctor(nombre, apellido, nacimiento, ci,lugar, sexo, especialidad,horario,cantpacientes);
        Hospital hospital = ControladorDatos.getController().getHospital();
        if(!existeCI(ci) && hospital.getDoctores().add(nuevoDoc)){
        
            ControladorDatos.getController().guardarObjeto();      
            res = true;
        }
        
        return res;
    }
    
    private boolean existeCI(String ciVerificar)
    {
      boolean respuesta = false;
      for(int i = 0; i < ControladorDatos.getController().getHospital().getDoctores().size() && !respuesta; i++)
      {
          if(ControladorDatos.getController().getHospital().getDoctores().get(i).getCI().equals(ciVerificar))
          {
              respuesta = true;
          }
      }
      for(int i = 0; i < ControladorDatos.getController().getHospital().getPacientes().size() && !respuesta; i++)
      {
          if(ControladorDatos.getController().getHospital().getPacientes().get(i).getcI().equals(ciVerificar))
          {
              respuesta = true;
          }
      }
      return respuesta;
    }
       
}

