/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.*;
import modelo.*;
import controlador.*;
/**
 *
 * @author DELL
 */

public class ControladorMedicamentos {
    
    private static final ControladorMedicamentos controladorMedicamentos = new ControladorMedicamentos();
    
    public static ControladorMedicamentos getController(){
        
        return controladorMedicamentos;
     }
    
     public void AñadirMedicamento(String nombre, int cantidad,String sirve,String peligro,String administracion){
       
        ControladorDatos.getController().getHospital().getMedicamento().add(new Medicamento(nombre,cantidad,sirve,peligro,administracion));
        ControladorDatos.getController().guardarObjeto();
     }
    public ArrayList<Medicamento> MedicamentosDisponibles(){
        
    return ControladorDatos.getController().getHospital().Disponibles();
    
    }
    
    public void AniadirCantMedicamento(String  m,int n){
        ControladorDatos.getController().getHospital().AniadirCantMedicamento(m, n);
        ControladorDatos.getController().guardarObjeto();
    }
    public void ConsumirMedicamento(String m,int n){
        ControladorDatos.getController().getHospital().ConsumirMedicamentos(m, n);
        ControladorDatos.getController().guardarObjeto();
    }

    }
    