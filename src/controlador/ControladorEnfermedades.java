/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.text.SimpleDateFormat;
import java.util.*;
import modelo.*;

/**
 *
 * @author hp
 */
public class ControladorEnfermedades {
    private static final ControladorEnfermedades controladorEnfermedades = new ControladorEnfermedades();
    
    public static ControladorEnfermedades getController(){
        return controladorEnfermedades;
    }
    

     public void RegistrarEnfermedad(String nombre,char tipo,ArrayList<String> sintomas){

    
        ControladorDatos.getController().getHospital().getEnfermedades().add(new Enfermedad(nombre,tipo,sintomas));
        ControladorDatos.getController().guardarObjeto();
    }
     
    public boolean esPandemia(Enfermedad enfermedad,Date fechaIni,Date fechaFin)
    {
        int numInfec = 0;
        boolean pandemia = false;
        for(int m=0;m<ControladorDatos.getController().getHospital().getPacientes().size();m++){
            for(int j=0;j<ControladorDatos.getController().getHospital().getPacientes().get(m).getTratamientos().size();j++){
                if(enfermedad.getNombre().equals(ControladorDatos.getController().getHospital().getPacientes().get(m).getTratamientos().get(j).getEnfermedad().getNombre())&&
                   enfermedad.getTipo()==(ControladorDatos.getController().getHospital().getPacientes().get(m).getTratamientos().get(j).getEnfermedad().getTipo())){
                      if(ControladorDatos.getController().getHospital().getPacientes().get(m).getTratamientos().get(j).getFecha().before(fechaFin)&&
                          ControladorDatos.getController().getHospital().getPacientes().get(m).getTratamientos().get(j).getFecha().after(fechaIni)){
                           numInfec++;
                        }
                }  
            }  
        }
        if(numInfec>1)
            pandemia=true;
        return pandemia;
    }
    
    public ArrayList<String> similares(ArrayList<String> sin){
        ArrayList<String> res = new ArrayList<String>();
        int similares = 0;
        for(int m = 0;m<ControladorDatos.getController().getHospital().getEnfermedades().size();m++){
          for(int y=0;y<ControladorDatos.getController().getHospital().getEnfermedades().get(m).getSintomas().size();y++){
              for(int j=0;j<sin.size();j++){
                 if(ControladorDatos.getController().getHospital().getEnfermedades().get(m).getSintomas().get(y).equals(sin.get(j))){
                       similares++ ;
                       break;   
                    }                   
              }
          }
             if(similares>2)
                 res.add(ControladorDatos.getController().getHospital().getEnfermedades().get(m).getNombre() + "   tipo: "+ControladorDatos.getController().getHospital().getEnfermedades().get(m).getTipo() );
             similares=0;
        }
        
        return res;
    }
    
    public ArrayList<String> enfermermos(Enfermedad e,Date fechaIni,Date fechaFin){
    ArrayList<String> res = new ArrayList<String>();
    SimpleDateFormat fe = new SimpleDateFormat("dd/MM/yyyy"); 
        for(int i=0;i<ControladorDatos.getController().getHospital().getPacientes().size();i++){
             for(int m=0;m<ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().size();m++){
                if (ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getEnfermedad().getNombre().equals(e.getNombre()) &&
                      ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getEnfermedad().getTipo()==(e.getTipo()))
                    if (ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha().before(fechaFin)&&
                          ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha().after(fechaIni)){ 
                            res.add(ControladorDatos.getController().getHospital().getPacientes().get(i).getNombreCompleto() + "--->" + fe.format(ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha()));
                     }
                }
             }
        return res;
    }
    
    public ArrayList<Infectado> pacientesEnfermos(Enfermedad e,Date fechaIni,Date fechaFin){
    ArrayList<Infectado> res = new ArrayList<Infectado>();
        for(int i=0;i<ControladorDatos.getController().getHospital().getPacientes().size();i++){
             for(int m=0;m<ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().size();m++){
                if (ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getEnfermedad().getNombre().equals(e.getNombre()) &&
                      ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getEnfermedad().getTipo()==(e.getTipo()))
                    if (ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha().before(fechaFin)&&
                          ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha().after(fechaIni)){
                            Infectado infAct=new Infectado(ControladorDatos.getController().getHospital().getPacientes().get(i),ControladorDatos.getController().getHospital().getPacientes().get(i).getTratamientos().get(m).getFecha());
                            res.add(infAct);
                     }
                }
             }
        return res;
    }

}