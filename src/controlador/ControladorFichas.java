/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.util.*;
import modelo.*;

/**
 *
 * @author Jhosh PC
 */
public class ControladorFichas {
    
    private static final ControladorFichas controladorFichas = new ControladorFichas();
    
    public static ControladorFichas getController(){
        return controladorFichas;
    }
    
    public void añadirFicha(Date fecha, Doctor doctor, Paciente paciente, String horario){     
        Ficha f = new Ficha(fecha, doctor, paciente, horario);
        ControladorDatos.getController().guardarObjeto();
    }
      
}
