/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.*;
import modelo.*;
import controlador.*;

/**
 *
 * @author DELL
 */

public class ControladorConsultas {
    
    private static final ControladorConsultas controladorConsultas = new ControladorConsultas();
    
    public static ControladorConsultas getcontroller(){
        return controladorConsultas;
    }
    
    public void registrarConsutla(Date fecha, Paciente paciente, Doctor doctor,String motivo,String recomendacion,double peso,int altura){
        Consulta n=new Consulta(fecha,paciente.getNombreCompleto(),doctor.getNombreCompleto(),motivo,recomendacion,peso,altura);
        ControladorDatos.getController().getHospital().getConsultas().add(n);
        paciente.getKardex().add(n);
        doctor.getConsultas().add(n);
        ControladorDatos.getController().guardarObjeto();
    }
    public void registrarTratamiento(Paciente p,Medicamento m,Date Duracion,Date Fecha,Enfermedad e,Doctor d, int cant, String prescripcion,String cuidados){
       Tratamiento n=new Tratamiento(p,m,Duracion,Fecha,e,d,cant,prescripcion,cuidados);
        ControladorDatos.getController().getHospital().getTratamientos().add(n);
       d.getTratamientos().add(n);
       p.getTratamientos().add(n);
       ControladorDatos.getController().guardarObjeto();
    }
   
}
