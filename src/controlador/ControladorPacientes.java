/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import modelo.*;

/**
 *
 * @author DELL
 */
public class ControladorPacientes {
    
    private static final ControladorPacientes controladorPacientes = new ControladorPacientes();
    
    public static ControladorPacientes getController()
    {
        return controladorPacientes;
    }
    

    public boolean RegistrarPaciente(String nombre,String Apellido,String nacimiento,String ci,String sexo,String tipodesangre,String domicilio,String ocupacion){
        boolean res = false;
        Hospital hospital = ControladorDatos.getController().getHospital();
       if(!existeCI(ci) && hospital.getPacientes().add(new Paciente(nombre, Apellido, nacimiento, ci, sexo, tipodesangre,domicilio,ocupacion))){
        
           ControladorDatos.getController().guardarObjeto();        
           res = true;
       }
       
       return res;  
    } 
    
    private boolean existeCI(String ciVerificar)
    {
      boolean respuesta = false;
      for(int i = 0; i < ControladorDatos.getController().getHospital().getDoctores().size() && !respuesta; i++)
      {
          if(ControladorDatos.getController().getHospital().getDoctores().get(i).getCI().equals(ciVerificar))
          {
              respuesta = true;
          }
      }
      for(int i = 0; i < ControladorDatos.getController().getHospital().getPacientes().size() && !respuesta; i++)
      {
          if(ControladorDatos.getController().getHospital().getPacientes().get(i).getcI().equals(ciVerificar))
          {
              respuesta = true;
          }
      }
      return respuesta;
    }
   
}
    
