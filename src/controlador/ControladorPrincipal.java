/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import controlador.*;

/**
 *
 * @author DELL
 */

public class ControladorPrincipal {
    
    private static final ControladorPrincipal controladorPrincipal = new ControladorPrincipal();
    
    public static ControladorPrincipal getController(){
        return controladorPrincipal;
    }
    
    private Hospital H;
    private ControladorDoctores D;
    private ControladorPacientes P;
    private ControladorMedicamentos M;
    private ControladorConsultas C;
    private ControladorDatos data;
    private ControladorEnfermedades E;
    private ControladorIngresos I;
    
    public ControladorPrincipal(){
        D=new ControladorDoctores();
        P=new ControladorPacientes();
        M=new ControladorMedicamentos();
        C=new ControladorConsultas();
        E=new ControladorEnfermedades();
        H=new Hospital();
       //data = new ControladorDatos();
        I=new ControladorIngresos();
    }
    
    public ControladorDoctores getControladorDoctores(){
        return D;
    }
    
    public ControladorPacientes getControladorPacientes(){
        return P;
    }
    
    public ControladorMedicamentos getControladorMedicamentos(){
        return M;
    }
    
    public ControladorConsultas getControladorConsultas(){
        return C;
    }
      
    public ControladorEnfermedades getControladorEnfermedades(){
        return E;
    }
       
    public ControladorDatos getControladorDatos(){
        return data;
    }
        
    public Hospital getHospital(){
        return H;
    }
    
    public ControladorIngresos getControladorIngresos(){
        return I;
    }

}
