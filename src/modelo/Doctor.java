/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.*;
import java.util.*;
import javax.swing.JTextArea;

/**
 *
 * @author DELL
 */
public class Doctor implements Serializable{
    private String nombre;
    private String apellido;
    private String nacimiento;
    private String ci;
    private String lugar;
    private String sexo;
    private String especialidad;
    private String horario;
    private int cantidadPaciente;
    private ArrayList<Consulta>consultas;
    private ArrayList<Tratamiento>tratamientos;
    private ArrayList<Ficha> fichas;
    private String nombreCompleto;
    
    public Doctor(String nombre,String apellido,String nacimiento,String ci,String lugar,String sexo,String especialidad,String horario, int cantpaciente){
        this.nombre= nombre;
        this.apellido=apellido;
        this.nacimiento=nacimiento;
        this.ci=ci;
        this.lugar=lugar;
        this.sexo=sexo;
        this.especialidad=especialidad;
        this.horario=horario;
        cantidadPaciente=cantpaciente;
        consultas=new ArrayList();
        tratamientos=new ArrayList();
        fichas=new ArrayList<Ficha>();
        nombreCompleto= nombre+" "+apellido;
        
    }
    
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
    return apellido;
    }
    public String getCI(){
    return ci;
    }
    public String getNacimiento(){
    return nacimiento;
    }
    public String getLugar(){
    return lugar;
    }
    public String getSexo(){
    return sexo;
    }
    public String getEspecialidad(){
    return especialidad;
    }
    public ArrayList<Consulta> getConsultas(){
    return consultas;
    }
    public ArrayList<Tratamiento> getTratamientos(){
    return tratamientos;
    }
    public String Mostrar(){
    String res=nombre+" "+apellido+" "+especialidad;
    return res;
    }
    public String  getNombreCompleto(){
    return nombreCompleto;
    }
    public String getHorario(){
        return horario;
    }
    public int getCantidadPacientes(){
        return cantidadPaciente;
    }

    public boolean estaOcupado(Date dia) {
        boolean respuesta = true;
        int cantidad=0;
        for(int i=0;i<fichas.size()&& cantidad<4;i++){
            if(fichas.get(i).getFecha().equals(dia)){
                cantidad++;
            }
        }
        if(cantidad<4){
            respuesta=false;
        }
        
        return respuesta;
    }
    
    @Override
    public String toString(){
        return nombreCompleto;
    }
     public ArrayList<Ficha> getFichas() {
        return fichas;
    }
    public void deleteFicha(Ficha f){
        fichas.remove(f);
    }
    public void addFicha(Ficha f){
        fichas.add(f);
    }   
    public void NuevaConsulta(Consulta c){
        consultas.add(c);
    }
}
