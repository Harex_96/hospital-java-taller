/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.ControladorDatos;
import modelo.*;
import java.util.*;
import java.io.*;

public class Hospital implements Serializable{
    
  private ArrayList<Doctor>doctores;
  private ArrayList<Paciente>pacientes;
  private ArrayList<Consulta>consultas;
  private ArrayList<Medicamento>medicamentos;
  private ArrayList<Enfermedad>enfermedades;
  private ArrayList<Tratamiento>tratamientos; 
  private Administrador administrador;
  private ArrayList<String>gestionMed;
  
  public Hospital(){
      
    doctores=new ArrayList();
    pacientes=new ArrayList();
    consultas=new ArrayList();
    medicamentos=new ArrayList();
    enfermedades=new ArrayList();
    tratamientos=new ArrayList();
    administrador = new Administrador();
    gestionMed= new ArrayList();
  }
  
  public Paciente buscarPaciente(String ci){
      Paciente respuesta = null;
      for (int i = 0; i< pacientes.size()&&respuesta==null;i++){
          if(pacientes.get(i).getcI().equals(ci)){
              respuesta = pacientes.get(i);
          }
      }
      return respuesta;
  }
  
  public Doctor buscarDotor(String ci){
      Doctor respuesta = null;
      for (int i = 0; i< doctores.size()&&respuesta==null;i++){
          if(doctores.get(i).getCI().equals(ci)){
              respuesta = doctores.get(i);
          }
      }
      return respuesta;
  }
  
  public Enfermedad buscarEnfermedad(String nombre)
    {
       Enfermedad respuesta = null;
      for (int i = 0; i< enfermedades.size()&&respuesta==null;i++){
          if(enfermedades.get(i).getNombre().equals(nombre)){
              respuesta = enfermedades.get(i);
          }
      }
      return respuesta;
    }
  
   public Enfermedad buscarEnfermedadCompleta(String nombre,char tipo)
    {
       Enfermedad respuesta = null;
      for (int i = 0; i< enfermedades.size()&&respuesta==null;i++){
          if(enfermedades.get(i).getNombre().equals(nombre) &&
                  enfermedades.get(i).getTipo() == tipo){
              respuesta = enfermedades.get(i);
          }
      }
      return respuesta;
    }
    
   public void aniadirSintomasNuevos(String nombre,char tipo,ArrayList<String> sin){
       buscarEnfermedadCompleta(nombre,tipo).aniadirSintomasN(sin);
   }
   
   public ArrayList<String> nombresEnfermedades(){
    ArrayList<String> res = new ArrayList<String>();
    boolean rev =false;
    if(enfermedades.isEmpty()==false){
    res.add(enfermedades.get(0).getNombre());
        for (int m=1;m<enfermedades.size();m++){
            for(int j=0;j<res.size();j++){
                if(res.get(j).equals(enfermedades.get(m).getNombre())==true){
                    rev=true;
                    break;
                }
            }
            if(rev==false){
                res.add(enfermedades.get(m).getNombre());
            }
            else
                rev=false;
        }
    }   
    return res;
   }
  
   
   public ArrayList<String> tipos(String nombre){
       ArrayList<String> res = new ArrayList<String>();
       for(int m=0;m<enfermedades.size();m++)
           if(enfermedades.get(m).getNombre().equals(nombre))
               res.add(String.valueOf(enfermedades.get(m).getTipo()));
       return res;
   }
   
    public int contarPacientesEnfe(Enfermedad enfermedad,Date fechaIni,Date fechaFin)
    {
        int numInfec = 0;
        boolean pandemia = false;
        for(int m=0;m<pacientes.size();m++){
            for(int j=0;j<pacientes.get(m).getTratamientos().size();j++){
                if(enfermedad.getNombre().equals(pacientes.get(m).getTratamientos().get(j).getEnfermedad().getNombre())&&
                   enfermedad.getTipo()==(pacientes.get(m).getTratamientos().get(j).getEnfermedad().getTipo())){
                      if(pacientes.get(m).getTratamientos().get(j).getFecha().before(fechaFin)&&
                          pacientes.get(m).getTratamientos().get(j).getFecha().after(fechaIni)){
                           numInfec++;
                        }
                }  
            }  
        }
        return numInfec;
    }
       
     
  public Paciente logeoPaciente(String user, String password){
      Paciente respuesta = null;
      for (int i = 0; i< pacientes.size()&&respuesta==null;i++){
          if(pacientes.get(i).getNombre().equals(user) && pacientes.get(i).getcI().equals(password)){
              respuesta = pacientes.get(i);
          }
      }
      return respuesta;
  }
  
   public Doctor logeoDoctor(String user, String password){
      Doctor respuesta = null;
      for (int i = 0; i< doctores.size()&&respuesta==null;i++){
          if(doctores.get(i).getNombre().equals(user) && doctores.get(i).getCI().equals(password)){
              respuesta = doctores.get(i);
          }
      }
      return respuesta;
  }
   
   public Administrador logeoAdministrador(String user, String password){
       Administrador respuesta = null;
       if(administrador.VerificarAdministrador(user, password))
       {
           respuesta = administrador;
       }
       return respuesta;
   }
  
  
  public ArrayList<Doctor> getDoctores(){
    return doctores;
  }
  
  public ArrayList<Paciente> getPacientes(){
    return pacientes;
  }
  
  public ArrayList<Consulta> getConsultas(){
    return consultas;
  }
  
  public ArrayList<Medicamento> getMedicamento(){
    return medicamentos;
  }
  
  public ArrayList<Enfermedad> getEnfermedades(){
    return enfermedades;
  }
  public ArrayList<Tratamiento> getTratamientos(){
      return tratamientos;
  }
  public ArrayList<Medicamento> Disponibles(){
   ArrayList<Medicamento>disponibles=new ArrayList();
   for(int i=0; i<medicamentos.size();i++){
    if(medicamentos.get(i).getCantida()>0){
        disponibles.add(medicamentos.get(i));
    }
   }
   return disponibles;
  }
  public void AniadirCantMedicamento(String n,int nu){
   BuscarMed(n).setCantidad(nu+BuscarMed(n).getCantida());
  }
  public void ConsumirMedicamentos(String n ,int nu){
     BuscarMed(n).setCantidad(BuscarMed(n).getCantida()-nu);
  }
  public Medicamento BuscarMed(String n){
   Medicamento respuesta=null;
      for (int i = 0; i< medicamentos.size()&&respuesta==null;i++){
          if(medicamentos.get(i).getNombre().equals(n)){
              respuesta = medicamentos.get(i);
          }
      }
      return respuesta;
  }
  public void NuevaConsulta(Consulta c){
      consultas.add(c);
  }
  public ArrayList<String> GestionMed(){
  return gestionMed;
  }
}
