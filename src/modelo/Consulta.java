package modelo;
import java.io.*;
import java.util.*;


public class Consulta implements Serializable{
    
   
    private Date fecha;
    private String paciente;
    private String doctor;
    private String Motivo;
    private String Recomendacion;
    private double Peso;
    private int Altura;
    
    
    public Consulta ( Date fecha, String paciente, String doctor,String motivo,String recomendacion,double peso, int altura){
        
        
        this.fecha = fecha;
        this.paciente = paciente;
        this.doctor = doctor;
        Motivo=motivo;
        Recomendacion= recomendacion;
        Peso=peso;
        Altura=altura;
    }
    
    public Date getFecha(){
        return fecha;
    }
    

    public String ResumenGeneral(){
        return "------------------------------------"+"\n"+"Fecha: "+fecha.toString()+"\n"+" Paciente: "+paciente+"\n"+" Doctor: "+doctor+"\n"+"Motivo de consulta: "+Motivo+"\n"+"Recomendacion: "+Recomendacion+"\n"+"Peso: "+String.valueOf(Peso)+"gr \n"+"Altura:"+String.valueOf(Altura)+"cm \n"+"------------------------------------"+"\n";
    }
}
