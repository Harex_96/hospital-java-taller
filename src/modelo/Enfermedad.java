/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.io.*;
import java.util.*;

import java.io.Serializable;
/**
 *
 * @author hp
 */
public class Enfermedad  implements Serializable{
    
    private String nombre;
    private char tipo ;
    private ArrayList<String> sintomas;
    
    public Enfermedad (String nombre, char tipo,ArrayList<String> sin){
    this.nombre=nombre;
    this.tipo=tipo;
    sintomas= sin;

  
    }
    
    public char getTipo(){
        return tipo;
    }
    
    public String getNombre(){
        return nombre;
    }
    
   
    
    public ArrayList<String> getSintomas(){
        return sintomas;
    }
    
    public void aniadirSintomasN(ArrayList<String> sinN){
        boolean ver=false;
       for (int m = 0; m<sinN.size(); m++){
           for(int j = 0; j<sintomas.size();j++){
               if(sintomas.get(j).equalsIgnoreCase(sinN.get(m))== true){
                   ver = true;
                   break;
               }
           }
           if(ver==false)
               sintomas.add(sinN.get(m));
           else 
               ver=false;
       }
    }
}
