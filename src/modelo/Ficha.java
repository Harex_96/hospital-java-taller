/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.*;


/**
 *
 * @author Jhosh PC
 */
public class Ficha implements Serializable{
    
    Date fecha;
    Doctor doctor;
    Paciente paciente;
    String horario;
    
    public Ficha(Date fecha, Doctor doctor, Paciente paciente, String horario){
        this.fecha=fecha;
        this.doctor=doctor;
        this.paciente=paciente;
        this.horario=horario;
        if(!doctor.estaOcupado(fecha) && paciente.f_Existente(fecha, doctor)){
            doctor.addFicha(this);
            paciente.addFicha(this);
        }else{
            System.out.println("No se pudo Registrar.");
        }
    }

    public Date getFecha() {
        return fecha;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public String getHorario() {
        return horario;
    }
    public String Mostrar(){
        int month = fecha.getMonth()+1;
        String res = "---------------------------------------------------------------------------------------"+ "\n" + "-- Fecha: " + fecha.getDate()+" / "+month+" / "+"2020" + "\n" + "-- Paciente: "+paciente.getNombreCompleto() + "\n" +" -- Doctor: ("+ doctor.getEspecialidad()+ ") "+ doctor.getNombreCompleto() + "\n" + "-- Horario: " + " ||" + horario + "|| " +"\n"+  "---------------------------------------------------------------------------------------";
        return res;
    }
    
}
