package modelo;

import java.io.*;
import java.util.*;

public class Paciente implements Serializable {

    private String nombre;
    private String apellido;
    private String nacimiento;
    private String cI;
    private String sexo;
    private String tipodesangre;
    private ArrayList<Consulta> kardex;
    private ArrayList<Tratamiento> tratamientos;
    private String NombreCompleto;
    private final String domicilio;
    private String ocupacion;
    private ArrayList<Ficha> fichas;

    public Paciente(String nombre, String apellido, String nacimiento, String ci, String sexo, String tipodesangre, String domicilio, String ocupacion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nacimiento = nacimiento;
        this.cI = ci;
        this.sexo = sexo;
        this.tipodesangre = tipodesangre;
        kardex = new ArrayList();
        tratamientos = new ArrayList();
        NombreCompleto = nombre + " " + apellido;
        this.domicilio = domicilio;
        this.ocupacion = ocupacion;
        fichas = new ArrayList();
    }

    public ArrayList<Consulta> getKardex() {
        return kardex;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public ArrayList<Tratamiento> getTratamientos() {
        return tratamientos;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public String getcI() {
        return cI;
    }

    public String getSexo() {
        return sexo;
    }

    public String getTipodesangre() {
        return tipodesangre;
    }

    public String Mostrar() {
        return nombre + " " + apellido;

    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public ArrayList<Ficha> getFichass() {
        return fichas;
    }

    public void deleteFicha(Ficha f) {
        fichas.remove(f);
    }

    public void addFicha(Ficha f) {
        fichas.add(f);
    }

    public boolean f_Existente(Date dia, Doctor doc) {
        boolean respuesta = true;
        int cantidad = 0;
            for (int i = 0; i < fichas.size() && cantidad < 2; i++) {
                if (fichas.get(i).getFecha().equals(dia) && fichas.get(i).getDoctor().getCI() == doc.getCI()) {
                    cantidad++;
                    if (cantidad < 2) {
                        respuesta = false;
                    }
                }
            }
        return respuesta;
    }
    

}
