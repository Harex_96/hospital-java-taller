/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Jhosh PC
 */
public class Tratamiento implements Serializable{
    
    
    Date fecha;
    Paciente paciente;
    Medicamento medicamento;
    Date Reconsulta;
    Enfermedad enfermedad;
    Doctor doctor;
    int Cant;
    String Prescripcion;
    String Cuidado;
    
    public Tratamiento(Paciente p,Medicamento m,Date reconsulta,Date Fecha,Enfermedad e,Doctor d, int cant,String prescripcion,String cuidado){
     fecha=Fecha;
     paciente=p;
     medicamento=m;
     enfermedad=e;
     doctor=d;
     Reconsulta=reconsulta;
     Cant=cant;
     Prescripcion=prescripcion;
     Cuidado=cuidado;
     
    }
    
    public Paciente getPaciente(){
        return paciente;
    }
    
    public Medicamento getMedicamento(){
        return medicamento;
    }
    
    public Date getReconsulta(){
        return Reconsulta;
    }
    
    public Date getFecha(){
        return fecha;
    }
    
    public Enfermedad getEnfermedad(){
        return enfermedad;
    }
    
    public Doctor getDoctor(){
        return doctor;
    }
    public int Cantidad(){
    return Cant;
    }
    public String MostrarResumen(){
    String res="-------------------------------------------"+"\n"+"inicio: "+fecha.toString()+"\n"+"Medicamento: "+medicamento.getNombre()+"\n"+"Reconsulta: "+Reconsulta.toString()+"\n"+"Doctor: "+doctor.getNombreCompleto()+"\n Paciente: "+paciente.getNombreCompleto()+"\n Enfermedad: "+enfermedad.getNombre()+"\n"+"-------------------------------------------"+"\n";
    return res;
    }
    public String MedicametoResetado(){
    String res= "-------------------------------------------"+"\n"+"Medicamento: "+medicamento.getNombre()+"\n"+"Cantidad: "+Cant+"\n"+"Paciente:"+paciente.getNombreCompleto()+"\n"+"Doctor: "+doctor.getNombreCompleto()+"\n"+"Fecha: "+fecha.toString()+"\n"+"-------------------------------------------"+"\n";
    return res;
    }
    public String Reconsulta(){
    String res= "-------------------------------------------"+"\n"+"Fecha :"+Reconsulta.toString()+"\n"+"Paciente: "+paciente.getNombreCompleto()+"\n"+"Doctor: "+doctor.getNombreCompleto()+"\n"+"-------------------------------------------"+"\n";
     return res;
    }
    public String Receta(){
    String res="---------------------------------------------- \n Fecha: "+fecha.toString()+"\n Paciente: "+paciente.getNombreCompleto()+ "\n Medicamento: "+medicamento.getNombre()+"\n Prescripcion: "+Prescripcion+"\n Cuidados:"+Cuidado+"\n  ---------------------------------------------- \n";
    return res;
    }
}
