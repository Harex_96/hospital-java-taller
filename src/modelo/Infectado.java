/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author hp
 */
public class Infectado implements Serializable {
    private Paciente paciente;
    private Date fecha;
    public Infectado(Paciente paciente,Date fecha)
    {
        this.paciente=paciente;
        this.fecha=fecha;
    }
    public Paciente getPaciente()
    {
        return paciente;
    }
    public Date getFecha()
    {
        return fecha;
    }    
}

