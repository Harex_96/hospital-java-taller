/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.io.*;


import java.io.Serializable;

/**
 *
 * @author hp
 */
public class Medicamento  implements Serializable{
    
    private String nombre;
    private int cantidad;
    private String Sirve;
    private String Peligro;
    private String Administracion;

    public Medicamento(String nombre,int cant,String sirve,String peligro,String administracion){
   
    this.nombre=nombre;
    cantidad=cant;
    Sirve=sirve;
    Peligro=peligro;
    Administracion=administracion;
    }
    public void setCantidad(int cant){
        cantidad=cant;
    }
    public String getNombre(){
        return nombre;
    }
    public int getCantida(){
        return cantidad;
    }
    
    public String Mostrarse(){
    String res="-----------------------------------\n Nombre: "+nombre+"\nCantidad: "+cantidad+"\n Sirve para: "+Sirve+"\n No usar en caso de: "+Peligro+"\n Via de administracion: "+Administracion+"\n"+"----------------------------------- \n";
    return res;
    }
}
